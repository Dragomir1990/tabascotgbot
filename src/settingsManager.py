# coding=UTF-8

class settingsManager:
    """This class is used to store and modify bot settings"""

    def __init__(self):
        self.myProfile = "👤Мой профиль"
        self.setClass = "🔮Установить свой класс"
        self.warriorClass = "⚔Нападающий"
        self.defenderClass = "🛡Защитник"
        self.greetingsMessage = "Добро пожаловать! Скинь мне форвардом свой профиль"
        self.adminGreetingsMessage = "Привет! Что сейчас будем делать?"
        self.settingsMainMessage = "Настроек не предусмотрено(пока что)"
        self.profileAnswerOkMessage = "Спасибо, я обновил твой профиль"
        self.profileAnswerTimedOutProfile = "Извини, это старое сообщение. Скинь более новую стату"
        self.profileAnswerWrongFortressMessage = "А не шпион-ли ты часом?;)"
        self.profileAnswerErrorMessage = "Ой, а форвард-то не оттуда, откуда надо"
        self.helpMessage = "Этот бот сделан для координации атак/защит отрядов красного замка в @ChatWarsBot"
        self.accessDeniedMessage = "Этот раздел не для простых воинов"
        self.unknownCommand = "Извини, я не могу понять, что ты мне написал"

        self.playersCommand = "👥Игроки"
        self.statCommand = "🏗Статистика"
        self.retextCommand = "🛠Изменение текстовок"
        self.actionsCommand = "‼️Команды"
        self.allBroadcastsCommand = "📢Объявления"
        self.kickCommand = "📇Кикнуть"
        self.deleteAdminCommand = "🗑Удалить админа"
        
        self.broadcastCommand = "📢Всем"
        self.spycastCommand = "🕵Шпионам"
        self.lowlevelcastCommand = "🥉Low-lvl"
        self.midlevelcastCommand = "🥈Mid-lvl"
        self.highlevelcastCommand = "🥇High-lvl"
        self.insquadcastCommand = "👤Отряду"
        self.allsquadscastCommand = "👥Всем отрядам"

        self.statAtkCommand = "🏗Статистика атаки"
        self.statDefCommand = "🏗Статистика защиты"
        self.statGoldCommand = "🏗Статистика по золоту"
        self.statLevelCommand = "🏗Статистика уровней"

        self.actionAttackBlue = "🇪🇺На синих!"
        self.actionAttackYellow = "🇻🇦На жёлтых!"
        self.actionAttackWhite = "🇨🇾На белых!"
        self.actionAttackBlack = "🇬🇵На чёрных!"
        self.actionAttackForestFort = "⚔🌲"
        self.actionAttackMountainsFort = "⚔⛰"
        self.actionDefense = "🛡Замка!"
        self.actionDefenseFriend = "🛡Союзника!"
        self.actionDefenseFort = "🛡Форта!"
        self.backCommand = "🔙Назад"

        self.acceptActionCommand = "⚔Я в деле"
        self.declineActionCommand = "❌Я пас"

        self.listAccepted = "📈Кто идёт?"
        self.listDeclined = "📉Кто против?"

        self.settingsStorage = dict([("lowlevelthreshold", 9), ("highlevelthreshold", 20)])