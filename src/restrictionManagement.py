# coding=UTF-8

class restrictionManager():
    """This class manages user access right to info stored in a bot"""

    def __init__(self):
        self.whiteList = dict([('82752992', True), ('322189325', True)])

    def addUser(self, telegramID):
        self.whiteList[str(telegramID)] = True

    def deleteUser(self, telegramID):
        return self.whiteList.pop(str(telegramID), None) is None
