import sqlite3


class DbWrapper:
    """This class is wrapper for SQLite3 database"""

    def __init__(self, database):
        self.connection = sqlite3.connect(database, check_same_thread=False)
        self.cursor = self.connection.cursor()

    def load_players(self):
        with self.connection:
            return self.cursor.execute('SELECT * FROM players').fetchall()

    def add_player(self, player, isSpy = False):
        with self.connection:
            query = "INSERT OR REPLACE INTO players VALUES ({0},\'{1}\', {2}, {3}, {4}, {5}, {6}, \'{7}\', {8}, \'{9}\', {10})".format(
                player.telegramID, player.name, int(player.fraction), player.level, player.attack, player.defense,
                player.gold, player.telegram_username, player.squad, player.type, int(isSpy))
            self.cursor.execute(query).fetchall()

    def delete_player(self, player):
        with self.connection:
            query = "DELETE FROM players WHERE telegramID = {0}".format(player)
            self.cursor.execute(query)

    def load_admins(self):
        with self.connection:
            query = "SELECT * FROM adminsList"
            result = self.cursor.execute(query).fetchall()
            parsed_result = dict([])
            for i in result:
                parsed_result[i[0]] = True
            return parsed_result

    def add_admin(self, telegramID):
        with self.connection:
            query = "INSERT OR REPLACE INTO adminsList VALUES ({0})".format(telegramID)
            self.cursor.execute(query)

    def delete_admin(self, telegramID):
        with self.connection:
            try:
                query = "DELETE FROM adminsList WHERE telegramID = {0}".format(telegramID)
                self.cursor.execute(query)
            except:
                print('ERROR[dbWrapper]: delete_admin() - wrong telegramID')
