# coding=UTF-8

import constants
from datetime import datetime
from datetime import timedelta

squad_list = dict([("Без_отряда",0), ("Tabasco", 1), ("Red_detachment_of_cats", 2), ("Красный_авангард", 3),
                   ("Мир_RED", 4), ("Хулингвин", 5), ("Красная_зима", 6), ("ЧыРвань", 7), ("ДВ", 8), ("15_шляп", 9)])

class Player:
    'Class, which describes player stats'

    def __init__(self):
        self.telegramID = 0
        self.name = 'name'
        self.fraction = False
        self.level = 1
        self.attack = 1
        self.defense = 1
        self.gold = 0
        self.telegram_username = ''
        self.update_time = 0
        self.action_agreement = 0
        self.type = "🔮"
        self.squad = 0
        self.state = ""

    def parseMessage(self, message):
        fw_time = datetime.fromtimestamp(message.forward_date)
        if not constants.isDebug:
            if (datetime.now() - fw_time) > timedelta(minutes=constants.playerProfileValidInMinutes):
                return False
        self.telegramID = message.from_user.id
        exploded_string = message.text.splitlines()
        while (len(exploded_string)!=0) and ("Битва пяти замков через" not in exploded_string[0]):
            del exploded_string[0]
        if len(exploded_string) >= 11:
            self.level = int(exploded_string[3].replace('🏅Уровень: ', ''))
            self.fraction = ', Воин Красного замка' in exploded_string[2]
            self.name = exploded_string[2].replace(', Воин Красного замка', '')
            stats = exploded_string[4].split('🛡')
            atk = stats[0].replace('⚔Атака: ', '')
            defe = stats[1].replace('Защита: ', '')
            if '+' in atk:
                self.attack = int(atk.split('+')[0]) + int(atk.split('+')[1])
            else:
                self.attack = int(atk.split('+')[0])
            if '+' in defe:
                self.defense = int(defe.split('+')[0]) + int(defe.split('+')[1])
            else:
                self.defense = int(defe.split('+')[0])
            if int(atk.split('+')[0]) > int(defe.split('+')[0]):
                self.type = '⚔'
            elif int(atk.split('+')[0]) < int(defe.split('+')[0]):
                self.type = '🛡'
            self.gold = int(exploded_string[6].replace('💰Золото: ', ''))
            self.telegram_username = (str(message.from_user.id), message.from_user.username)[message.from_user.username is not None]
            while (len(exploded_string)!=0) and ("Состояние:" not in exploded_string[0]):
                del exploded_string[0]
            self.state = exploded_string[1]
            print(self.state)
            return True
        else:
            return False


    def printPlayer(self):
        print('______________________\nName: {0}'.format(self.name))
        print('Fortress: {0}'.format(self.fraction))
        print('Level: {0}'.format(self.level))
        print('Attack: {0} Def: {1}'.format(self.attack, self.defense))
        print('Gold: {0}'.format(self.gold))
        print('tgID: {0}\ntgUsername: {1}\n______________________\n'.format(self.telegramID, self.telegram_username))

    def getAsString(self):
        return 'tgID: {5}\nИмя: {0}\nКрасный-ли:{1}\nУровень: {2}\nАтака: {3}\nЗащита: {4}\nЗолото:{6}'.format(
            self.name, self.fraction, self.level, self.attack, self.defense, self.telegramID, self.gold)

    def getAsLine(self, show_tg_id = False):
        squadname = ''
        for squad in squad_list:
            if squad_list[squad] == self.squad:
                squadname = squad
        line = '{0}{1}\t🏅{2} ⚔{3} 🛡{4} @{5} Отряд:{6}'.format(self.type, self.name, self.level, self.attack, self.defense,
                                                     self.telegram_username, squadname.replace('_', ' '))
        if show_tg_id:
            line +=' TelegramID: {0}\n'.format(self.telegramID)
        else:
            line += '\n'
        return line

    def getDetailedTextInfo(self):
        squadname = ''
        for squad in squad_list:
            if squad_list[squad] == self.squad:
                squadname = squad
        player_class = '🔮Нейтральный'
        if self.type == '⚔':
            player_class = '⚔Атакер'
        elif self.type == '🛡':
            player_class = '🛡Дефер'
        return  '{0}, 🏅{1}-го уровня\n' \
                '🔰Класс: {2}\n' \
                '⚔Атака: {3}\n' \
                '🛡Защита: {4}\n' \
                '💰Золото: {5}\n' \
                '👥Отряд: {6}\n' \
                '📋ID: {7}\n' \
                '{8}'.format(self.name, self.level, player_class, self.attack, self.defense, self.gold, squadname.replace('_',' '), self.telegramID, self.state)