import playerClass
import settingsManager


def atk_functor(player, settings, attack):
    return (False, player.attack >= int(attack))[player is not None]


def def_functor(player, settings, defense):
    return (False, player.defense >= int(defense))[player is not None]


def lvl_functor(player, settings, lvl):
    return (False, player.level >= int(lvl))[player is not None]


def low_lvl_functor(player, settings, lvl):
    return (False, player.level <= settings.settingsStorage["lowlevelthreshold"])[player is not None]


def mid_lvl_functor(player, settings, lvl):
    return (False, (player.level > settings.settingsStorage["lowlevelthreshold"])
        and (player.level < settings.settingsStorage["highlevelthreshold"]))[player is not None]


def high_lvl_functor(player, settings, lvl):
    return (False, player.level >= settings.settingsStorage["highlevelthreshold"])[player is not None]


def atk_type_functor(player, settings, value = 0):
    return (False, player.type == "⚔")[player is not None]


def def_type_functor(player, settings, value = 0):
    return (False, player.type == "🛡")[player is not None]


def is_in_squad_functor(player, settings, squadID):
    return (False, player.squad == playerClass.squad_list.get(squadID, 0))[player is not None]

def in_any_squad_functor(player, settings, value = 0):
    return(False, player.squad != 0)[player is not None]

def not_in_squad_functor(player, settings, value = 0):
    return (False, player.squad == 0)[player is not None]


def get_by_username_functor(player, settings, username):
    return (False, player.telegram_username.lower() == username.lower())[(player is not None) and (username is not None)]
    


def get_by_tg_id_functor(player, settings, tgid):
    return (False, player.telegramID == tgid)[player is not None]
