# coding=UTF-8
import re

import telebot

import constants
import playerClass
import settingsManager
import restrictionManagement
import databaseWrapper
import msUtil

playerList = []
spy_list = []
deleted_list = []

db = databaseWrapper.DbWrapper('db/pers.db')

settings = settingsManager.settingsManager()
restrictions = restrictionManagement.restrictionManager()

defaultMarkup = telebot.types.ReplyKeyboardMarkup(True, False)
defaultMarkup.row(settings.myProfile)
defaultMarkup.row('/help')

setClassMarkup = telebot.types.ReplyKeyboardMarkup(True, False)
setClassMarkup.row(settings.warriorClass)
setClassMarkup.row(settings.defenderClass)

adminMarkup = telebot.types.ReplyKeyboardMarkup(True, False)
adminMarkup.row(settings.playersCommand, settings.statCommand)
adminMarkup.row(settings.retextCommand, settings.actionsCommand)
adminMarkup.row(settings.listAccepted, settings.listDeclined)
adminMarkup.row(settings.allBroadcastsCommand, settings.kickCommand, settings.deleteAdminCommand)

attackMarkup = telebot.types.ReplyKeyboardMarkup(True, False)
attackMarkup.row(settings.actionAttackBlue, settings.actionAttackYellow)
attackMarkup.row(settings.actionAttackWhite, settings.actionAttackBlack)
attackMarkup.row(settings.actionAttackForestFort, settings.actionAttackMountainsFort, settings.actionDefenseFort)
attackMarkup.row(settings.actionDefense, settings.actionDefenseFriend, settings.backCommand)

broadcastsMarkup = telebot.types.ReplyKeyboardMarkup(True, False)
broadcastsMarkup.row(settings.broadcastCommand, settings.spycastCommand)
broadcastsMarkup.row(settings.lowlevelcastCommand, settings.midlevelcastCommand, settings.highlevelcastCommand)
broadcastsMarkup.row(settings.insquadcastCommand, settings.allsquadscastCommand)
broadcastsMarkup.row(settings.backCommand)

commandReplyMarkup = telebot.types.ReplyKeyboardMarkup(True, False)
commandReplyMarkup.row(settings.acceptActionCommand, settings.declineActionCommand)


def mass_sender(text, markup=None):
    success = 0
    errors = 0
    for i in playerList:
        send_result = ""
        try:
            send_result = str(bot.send_message(i.telegramID, text, reply_markup=markup))
            success += 1
        except:
            error_report = "Ooops... This user caused an error in mass send:\n{0}\n Error info:\n{1}".format(
                i.getAsLine(), send_result)
            print(error_report)
            bot.send_message(constants.drake1990_tg_id, error_report)
            errors += 1
    return "Отправлено {0} сообщений, {1} ошибок".format(success, errors)


def parametrized_mass_sender(cmd, value, text, markup=None):
    matched_players = []
    selector = None
    answer = "Проблемы отправки параметризированного массового сообщения"
    if value is not None:
        if cmd == 'lvl':
            selector = msUtil.lvl_functor
        elif cmd == 'atk':
            selector = msUtil.atk_functor
        elif cmd == 'def':
            selector = msUtil.def_functor
        elif cmd == 'lowlevel':
            selector = msUtil.low_lvl_functor
        elif cmd == 'midlevel':
            selector = msUtil.mid_lvl_functor
        elif cmd == 'highlevel':
            selector = msUtil.high_lvl_functor
        elif cmd == 'attacker':
            selector = msUtil.atk_type_functor
        elif cmd == 'defender':
            selector = msUtil.def_type_functor
        elif cmd == 'notinsquad':
            selector = msUtil.not_in_squad_functor
        elif cmd == 'insquad':
            selector = msUtil.is_in_squad_functor
        elif cmd == 'allsquads':
            selector = msUtil.in_any_squad_functor

        answer = "Сообщение было отправлено следующим игрокам:\n"
        matched_players += get_player_list_by_criteria(selector, value)
        for player in matched_players:
            send_result = ""
            try:
                send_result = str(bot.send_message(player.telegramID, text, reply_markup=markup))
                answer += player.getAsLine()
            except:
                error_report = "Ooops... This user caused an error in mass send:\n{0}\n Error info:\n{1}".format(
                    player.getAsLine(), send_result)
                print(error_report)
                bot.send_message(constants.drake1990_tg_id, error_report)
    return answer


def load_players():
    playerList.clear()
    spy_list.clear()
    value = db.load_players()
    for i in value:
        player = playerClass.Player()
        player.telegramID = i[0]
        player.name = i[1]
        player.fraction = i[2]
        player.level = i[3]
        player.attack = i[4]
        player.defense = i[5]
        player.gold = i[6]
        if i[7] == 'None':
            player.telegram_username = str(i[0])
        else:
            player.telegram_username = i[7]
        if i[8] is not None:
            player.squad = i[8]
        if i[9] is not None:
            player.type = i[9]
        if i[10]:
            spy_list.append(player)
        else:
            playerList.append(player)
    return len(value)


bot = telebot.TeleBot(constants.token)

load_players()
restrictions.whiteList = db.load_admins()

print(bot.get_me())


def log(message):
    print("\n ------")
    from datetime import datetime
    print(datetime.now())
    print("Сообщение от {0} {1} @{4}. (id = {2}) \n Текст = {3}".format(message.from_user.first_name,
                                                                   message.from_user.last_name,
                                                                   str(message.from_user.id), message.text, message.from_user.username))


@bot.message_handler(commands=['start'])
def handle_start(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        answer = settings.adminGreetingsMessage
        bot.send_message(message.chat.id, answer, reply_markup=adminMarkup)
    else:
        answer = settings.greetingsMessage
        bot.send_message(message.chat.id, answer, reply_markup=defaultMarkup)


@bot.message_handler(commands=['me'])
def handle_me_command(message):
    log(message)
    answer = 'Не могу тебя найти :(\nСкинь мне свой профиль'
    markup = telebot.types.ReplyKeyboardMarkup(True, False)
    for player in playerList:
        if player.telegramID == message.from_user.id:
            answer = player.getDetailedTextInfo()
            if player.type == '🔮':
                markup.row('🔮Установить свой класс')
            else:
                markup = (defaultMarkup, adminMarkup)[restrictions.whiteList.get(str(message.from_user.id)) == True]
    for spy in spy_list:
        if spy.telegramID == message.from_user.id:
            answer = spy.getDetailedTextInfo()
            if spy.type == '🔮':
                markup.row('🔮Установить свой класс')
            else:
                markup = (defaultMarkup, adminMarkup)[restrictions.whiteList.get(str(message.from_user.id)) == True]
    bot.send_message(message.chat.id, answer, reply_markup=markup)


@bot.message_handler(commands=['reloadbase'])
def handle_reloadbase_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        counter = load_players()
        bot.send_message(message.chat.id, "Загружено {0} профил(я/ей) игроков".format(counter))


@bot.message_handler(commands=['resavebase'])
def handle_resavebase_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        counter = 0
        for player in playerList:
            db.add_player(player)
            counter += 1
        for spy in spy_list:
            db.add_player(spy, True)
            counter += 1
        bot.send_message(message.chat.id, "Сохранено {0} профил(я/ей) игроков".format(counter))


@bot.message_handler(commands=['help'])
def handle_help_command(message):
    if restrictions.whiteList.get(str(message.from_user.id)):
        log(message)
        answer = "Список команд бота:\n" \
                 "/start - показать приветствие \n" \
                 "/reloadbase - перезагрузить базу игроков \n" \
                 "/resavebase - пересохранить базу игроков \n" \
                 "/players - список игроков в замке \n" \
                 "/kick <username 1> ... <username n> - удалить игроков \n" \
                 "/spy <username> - занести игрока в список шпионов\n" \
                 "/deleteadmin - удалить из администраторов \n" \
                 "/broadcast <atk,def,lvl> <критерий отбора> <текст> - отправить сообщение игрокам по критерию \n" \
                 "/listsquads - список отрядов \n" \
                 "/setsquad - <название отряда> <username 1> ... <username n> - добавить игроков в отряд \n" \
                 "/squad <сообщение> для обычной рассылки по отряду\n" \
                 "/squadvote <сообщение> для рассылки по отряду с подтверждением \n" \
                 "/attack - объявить атаку \n" \
                 "/getstat - получить статистику по всему замку \n" \
                 "/getplayer <atk,def,lvl> <критерий отбора> - получить информацию об игроках по критерию \n" \
                 "\n" \
                 "Ниже находится функционал в разработке: \n" \
                 "/settings - настройки (пока что не работают)\n" \
                 "/change - изменить тексты сообщений (недоделано) \n" \
                 "/lowlevel - Эта команда используется для массовой рассылки игрокам с низким уровнем (это будет " \
                 "установлено в настроках, пока не работает) \n" \
                 "/highlevel - Эта команда используется для массовой рассылки игрокам с высоким уровнем (это будет " \
                 "установлено в настроках, пока не работает) \n " \
                 "\n" \
                 "Бот работает в тестовом режиме. Свои замечания и предложения отправляйте @Drake1990 и @Doomed"

    else:
        answer = settings.helpMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['accept', 'decline'])
def handle_action_recievement_command(message):
    log(message)
    answer = "Извини, а ты кто?"
    for player in playerList:
        if player.telegramID == message.from_user.id:
            if 'accept' in message.text:
                player.action_agreement = 1
            elif 'decline' in message.text:
                player.action_agreement = -1
            answer = "Хорошо, я записал твой ответ"
    if restrictions.whiteList.get(str(message.from_user.id)):
        bot.send_message(message.chat.id, answer, reply_markup=adminMarkup)
    else:
        bot.send_message(message.chat.id, answer, reply_markup=defaultMarkup)


@bot.message_handler(commands=['accepted', 'declined'])
def handle_players_decision_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        splitted_answer = []
        if 'accepted' in message.text:
            term = 1
            answer = "Список игроков принявших приказ:\n"
        else:
            term = -1
            answer = "Список игроков отказавшихся от приказа:\n"

        sum_attack = 0
        sum_defense = 0
        players_count = 0
        for player in playerList:
            if player.action_agreement == term:
                if len(answer) > 3000:
                    splitted_answer.append(answer)
                    answer = ""
                answer += player.getAsLine()
                sum_attack += player.attack
                sum_defense += player.defense
                players_count += 1
        answer += "___________________\n⚔{0} 🛡{1} от {2} игроков".format(sum_attack, sum_defense, players_count)
        splitted_answer.append(answer)
        for text in splitted_answer:
            bot.send_message(message.chat.id, text, adminMarkup)
    else:
        bot.send_message(message.chat.id, settings.accessDeniedMessage, reply_markup=defaultMarkup)


@bot.message_handler(commands=['back'])
def handle_back_command(message):
    log(message)
    answer = "ОК"
    if restrictions.whiteList.get(str(message.from_user.id)):
        bot.send_message(message.chat.id, answer, reply_markup=adminMarkup)
    else:
        bot.send_message(message.chat.id, answer, reply_markup=defaultMarkup)


@bot.message_handler(commands=['settings'])
def handle_settings_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        answer = settings.settingsMainMessage
        log(message)
        bot.send_message(message.chat.id, answer)
    else:
        answer = settings.accessDeniedMessage
    log(message)
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['kick'])
def handle_kick_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        params = message.text.split(' ')
        if len(params) >= 2:
            answer = "Удалены следующие игроки:\n"
            del params[0]
            matched_players = []
            for username in params:
                if username is not None:
                    matched_players += get_player_list_by_criteria(msUtil.get_by_username_functor, username.lower())
            for player in matched_players:
                if player is not None:
                    answer += "@{0}\n".format(player.telegram_username)
                    deleted_list.append(player)
                    playerList.remove(player)
                    db.delete_player(player.telegramID)
        else:
            answer = "Для удаления игроков напишите /kick <username1> <username2> ... <username N>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['spy'])
def handle_spy_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        params = message.text.split(' ')
        if len(params) >= 2:
            was_spyed = False
            for i in playerList:
                if params[1].lower() == i.telegram_username.lower():
                    spy_list.append(i)
                    playerList.remove(i)
                    db.add_player(i, True)
                    was_spyed = True
                    answer = "В шпионы был записан {0}".format(i.telegram_username)
                    break
            if not was_spyed:
                answer = "Не найден пользователь с таким ником"
        else:
            answer = "Для занесения игрока в список шпионов введите /spy <telegram username>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)

@bot.message_handler(commands=['unspy'])
def handle_unspy_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        params= message.text.split(' ')
        if len(params) >= 2:
            was_unspyed = False
            for i in spy_list:
                if params[1].lower() == i.telegram_username.lower():
                    playerList.append(i)
                    spy_list.remove(i)
                    db.add_player(i, False)
                    was_unspyed = True
                    answer = "Из шпионов был удалён {0}".format(i.telegram_username)
                    break
            if not was_unspyed:
                answer = "Не найден пользователь с таким ником"
        else:
            answer = "Для удаления игрока из списка шпионов введите /unspy <telegram username>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)

@bot.message_handler(commands=['listspies'])
def handle_list_spies_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        answer = 'Список шпионов:\n'
        splitted_answer = []
        for i in spy_list:
            if len(answer) > 3000:
                splitted_answer.append(answer)
                answer = ""
            answer += i.getAsLine()
        answer += "___________________\n{0} шпионов забокировано".format(len(spy_list))
        splitted_answer.append(answer)
        for text in splitted_answer:
            bot.send_message(message.chat.id, text)
    else:
        bot.send_message(message.chat.id, settings.accessDeniedMessage, reply_markup=defaultMarkup)    


@bot.message_handler(commands=['addmeasadmin'])
def handle_addmeasadmin_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if message.forward_from is not None:
            restrictions.addUser(message.forward_from.id)
            db.add_admin(str(message.forward_from.id))
            answer = "Пользователь добавлен в администраторы"
        else:
            answer = "Скинь мне, пожалуйста, форвард от человека которого нужно добавить админом с текстом " \
                     "/addmeasadmin "
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['deleteadmin'])
def handle_deleteadmin_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        params = message.text.split(' ')
        if len(params) > 1:
            answer = "Этот человек будет лишён административных прав в боте. Если его нужно удалить из списка " \
                     "рассылки - вопрользуйтесь командой /kick "
            if restrictions.deleteUser(params[1]):
                db.delete_admin(params[1])
            else:
                answer = "Указан неправильный идентификатор"
        else:
            answer = "Список TelegramID добавленных в качестве администраторов\n"
            for admin in restrictions.whiteList:
                tgID = str(admin)
                username = 'Не сохранено'
                for user in playerList:
                    if user.telegramID == int(admin):
                        username = user.telegram_username
                        break
                answer += '{0} - @{1}\n'.format(tgID, username)
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['iamattacker'])
def handle_iamattacker_command(message):
    log(message)
    answer = "Извини, ты не можешь сменить профессию"
    users = get_player_list_by_criteria(msUtil.get_by_tg_id_functor, message.from_user.id)
    if len(users) >= 1:
        if users[0].type == '🔮':
            users[0].type = '⚔'
            answer = "Ты успешно изменил профессию"
            db.add_player(users[0])
    if restrictions.whiteList.get(str(message.from_user.id)):
        bot.send_message(message.chat.id, answer, reply_markup=adminMarkup)
    else:
        bot.send_message(message.chat.id, answer, reply_markup=defaultMarkup)


@bot.message_handler(commands=['iamdefender'])
def handle_iamdefender_command(message):
    log(message)
    answer = "Извини, ты не можешь сменить профессию"
    users = get_player_list_by_criteria(msUtil.get_by_tg_id_functor, message.chat.id)
    if len(users) >= 1:
        if users[0].type == '🔮':
            users[0].type = '🛡'
            answer = "Ты успешно изменил профессию"
            db.add_player(users[0])
    if restrictions.whiteList.get(str(message.from_user.id)):
        bot.send_message(message.chat.id, answer, reply_markup=adminMarkup)
    else:
        bot.send_message(message.chat.id, answer, reply_markup=defaultMarkup)


@bot.message_handler(commands=['listsquads'])
def handle_list_squads_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        answer = "Текущий список отрядов:\n"
        for squad in playerClass.squad_list:
            answer += "   {0}\n".format(str(squad), playerClass.squad_list[str(squad)])
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['setsquad'])
def handle_setsquad_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        exploded_text = message.text.split(' ')
        if len(exploded_text) >= 3:
            answer = "Изменена принадлежность к отряду у следующих игроков:\n"
            del exploded_text[0]
            matched_players = []
            squadname = exploded_text.pop(0)
            for username in exploded_text:
                matched_players += get_player_list_by_criteria(msUtil.get_by_username_functor, username)
            for player in matched_players:
                player.squad = playerClass.squad_list.get(squadname, 0)
                db.add_player(player)
                answer += player.getAsLine()
        else:
            answer = "Этой командой можно добавить одного, либо несколько игроков в отряд.\n" \
                     "Для добавления напишите эту команду со следующими параметрами:\n" \
                     "/setsquad <название отряда> <игрок 1> <игрок 2> ... <игрок N>\n" \
                     "Названия отрядов можно посмотреть с помощью команды /listsquads"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['listbroadcasts'])
def handle_listbroadcasts_command(message):
    log(message)
    is_admin = restrictions.whiteList.get(str(message.from_user.id))
    if is_admin:
            answer = "На данный момент есть такие возможности широковещания:"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer, reply_markup=(None, broadcastsMarkup)[is_admin])

@bot.message_handler(commands=['broadcast'])
def handle_broadcast_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if ' ' in message.text:
            message.text = message.text.replace('/broadcast ', '')
            answer = mass_sender(message.text)
        else:
            answer = "Эта команда используется для массовой рассылки всем игрокам(кроме шпионов):\n" \
                     "/broadcast <сообщение>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)

@bot.message_handler(commands=['allsquads'])
def handle_allsquads_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if ' ' in message.text:
            message.text = message.text.replace('/allsquads ', '')
            answer = parametrized_mass_sender('allsquads',0, message.text)
        else:
            answer = "Эта команда используется для массовой рассылки игрокам, состоящим в любом отряде:\n" \
                     "/allsquads <сообщение>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)    

@bot.message_handler(commands=['squad', 'squadvote'])
def handle_squad_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if ' ' in message.text:
            sender = None
            for player in playerList:
                if player.telegramID == message.chat.id:
                    sender = player
            if sender is not None:
                squad = sender.squad
                vote = 'squadvote' in message.text
                if vote:
                    text = message.text.replace('/squadvote', '', 1)
                else:
                    text = message.text.replace('/squad', '', 1)
                answer = parametrized_mass_sender('insquad', squad, text, (None, commandReplyMarkup)[vote])
            else:
                answer = "Ой, а я не знаю какой у тебя отряд"
        else:
            answer = "Эта команда используется для массовой рассылки внутри своего отряда:\n" \
                     "/squad <сообщение> для обычной рассылки либо \n" \
                     "/squadvote <сообщение> для рассылки с подтверждением"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['spycast'])
def handle_spycast_broadcast_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if ' ' in message.text:
            text = message.text.replace('/spycast', '', 1)
            answer = "Деза разослана следующим шпионам:\n"
            for spy in spy_list:
                send_result = ""
                try:
                    send_result = str(bot.send_message(spy.telegramID, text))
                    answer += spy.getAsLine()
                except:
                    error_report = "Ooops... This user caused an error in mass send:\n{0}\n Error info:\n{1}".format(
                        spy.getAsLine(), send_result)
                    print(error_report)
                    bot.send_message(constants.drake1990_tg_id, error_report)
        else:
            answer = "Эта команда используется для рассылки дезинформации шпионам :\n" \
                     "/spycast <сообщение>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['lowlevel', 'midlevel', 'highlevel'])
def handle_lowlevel_broadcast_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if ' ' in message.text:
            if '/lowlevel ' in message.text:
                text = message.text.replace('/lowlevel ', '', 1)
                answer = parametrized_mass_sender('lowlevel', 0, text)
            elif '/midlevel ' in message.text:
                text = message.text.replace('/midlevel ', '', 1)
                answer = parametrized_mass_sender('midlevel', 0, text)
            elif '/highlevel ' in message.text:
                text = message.text.replace('/highlevel ', '', 1)
                answer = parametrized_mass_sender('highlevel', 0, text)
        else:
            if '/lowlevel' in message.text:
                answer = "Эта команда используется для массовой рассылки игрокам с уровнем <={0} :\n" \
                     "/lowlevel <сообщение>".format(settings.settingsStorage["lowlevelthreshold"])
            elif '/midlevel' in message.text:
                answer = "Эта команда используется для массовой рассылки игрокам с уровнем >{0}, но <{1} :\n" \
                     "/midlevel <сообщение>".format(settings.settingsStorage["lowlevelthreshold"], settings.settingsStorage["highlevelthreshold"])
            elif '/highlevel' in message.text:
                answer = "Эта команда используется для массовой рассылки игрокам с уровнем >{0} :\n" \
                     "/highlevel <сообщение>".format(settings.settingsStorage["highlevelthreshold"])
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['atk'])
def handle_attackers_broadcast_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if ' ' in message.text:
            text = message.text.replace('/atk', '', 1)
            answer = parametrized_mass_sender('attacker', 0, text)
        else:
            answer = "Эта команда используется для массовой рассылки атакерам:\n" \
                     "/atk <сообщение>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['def'])
def handle_defenders_broadcast_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        if ' ' in message.text:
            text = message.text.replace('/def', '', 1)
            answer = parametrized_mass_sender('defender', 0, text)
        else:
            answer = "Эта команда используется для массовой рассылки деферам:\n" \
                     "/def <сообщение>"
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(commands=['players'])
def handle_players_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        show_tg_id = 'tgid' in message.text
        answer = 'Список игроков в замке:\n'
        splitted_answer = []
        sum_attack = 0
        sum_defense = 0
        for i in playerList:
            if len(answer) > 3000:
                splitted_answer.append(answer)
                answer = ""
            answer += i.getAsLine(show_tg_id)
            sum_attack += i.attack
            sum_defense += i.defense
        answer += "___________________\n⚔{0} 🛡{1} от {2} игроков".format(sum_attack, sum_defense, len(playerList))
        splitted_answer.append(answer)
        for text in splitted_answer:
            bot.send_message(message.chat.id, text)
            # else:
            #     bot.send_message(message.chat.id, answer)
    else:
        bot.send_message(message.chat.id, settings.accessDeniedMessage, reply_markup=defaultMarkup)


@bot.message_handler(commands=['attack'])
def handle_attack_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        bot.send_message(message.chat.id, "На кого идём? Или может защищаемся?", reply_markup=attackMarkup)
    else:
        bot.send_message(message.chat.id, settings.accessDeniedMessage, reply_markup=defaultMarkup)


@bot.message_handler(commands=['attack_blue', 'attack_yellow', 'attack_white', 'attack_black', 'attack_forest_fort',
                               'attack_mountain_fort' 'def_castle', 'def_friend_castle', 'def_fort'])
def handle_defined_attacks_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        for player in playerList:
            player.action_agreement = 0
        bot.send_message(message.chat.id, "Команду принял", reply_markup=adminMarkup)
        answer = 'никого'
        if 'attack_blue' in message.text:
            answer = "⚔Идём в атаку на {0}!".format("🇪🇺синих")
        elif 'attack_yellow' in message.text:
            answer = "⚔Идём в атаку на {0}!".format("🇻🇦жёлтых")
        elif 'attack_white' in message.text:
            answer = "⚔Идём в атаку на {0}!".format("🇨🇾белых")
        elif 'attack_black' in message.text:
            answer = "⚔Идём в атаку на {0}!".format("🇬🇵чёрных")
        elif 'attack_forest_fort' in message.text:
            answer = "⚔Идём в атаку на {0}!".format("🌲лесной форт")
        elif 'attack_mountain_fort' in message.text:
            answer = "⚔Идём в атаку на {0}!".format("⛰горный форт")
        elif 'def_castle' in message.text:
            answer = "🛡Защищаем замок!"
        elif 'def_friend_castle' in message.text:
            answer = "Защищаем замок союзника!"
        elif 'def_fort' in message.text:
            answer = "Защищаем наш форт!"
        mass_sender(answer, commandReplyMarkup)
    else:
        bot.send_message(message.chat.id, settings.accessDeniedMessage, reply_markup=defaultMarkup)


@bot.message_handler(commands=['getstat'])
def handle_getstat_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        answer = ""
        attack_stat = 0
        defend_stat = 0
        attackers_atk = 0
        defenders_def = 0
        gold_stat = 0
        level_stat = 0
        for player in playerList:
            attack_stat += player.attack
            defend_stat += player.defense
            gold_stat += player.gold
            level_stat += player.level
            if player.type == '⚔':
                attackers_atk += player.attack
            if player.type == '🛡':
                defenders_def += player.defense
        if len(playerList) != 0:
            level_stat = int(level_stat / len(playerList))
        else:
            level_stat = 0
        answer = "Общая атака по замку:{0} \n" \
                 "Общая защита по замку:{1} \n" \
                 "Общее золото по замку:{2} \n" \
                 "Общая атака атакеров:{3} \n" \
                 "Общая защита деферов:{4} \n" \
                 "Средний уровень по отряду:{5}".format(attack_stat, defend_stat, gold_stat, attackers_atk, defenders_def, level_stat)
        bot.send_message(message.chat.id, answer, reply_markup=adminMarkup)
    else:
        answer = settings.accessDeniedMessage
        bot.send_message(message.chat.id, answer)


def get_player_list_by_criteria(functor, value):
    matched_players = []
    if (functor is not None) and (value is not None):
        for player in playerList:
            if functor(player, settings, value):
                matched_players.append(player)
    return matched_players


# this method needs total rethink and review
@bot.message_handler(commands=['getplayer'])
def handle_getplayer_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        params = message.text.split(' ')
        splitted_answer = []
        matched_players = []
        selector = None
        del params[0]
        if len(params) is 0:
            answer = "Для получения списка игроков по критерию введите критерии, по которым вам необходима информация\n" \
                  "Возможна выборка по атаке, защите и уровню\n" \
                  "Формат следующий: /getplayer <atk,def,lvl,usr> <критерий отбора>\n" \
                  "Примеры:\n" \
                  "/getplayer atk 35\n" \
                  "/getplayer def 30\n" \
                  "/getplayer lvl 20\n" \
                  "/getplayer usr doomed" \
                  "/getplayer squad Tabasco"
            cmd = None
            value = None
        else:
            cmd = params.pop(0).lower()
            value = params.pop(0)

        answer = 'Список игроков подходящих под ваш запрос: \n'
        if (value is not None) and (cmd is not None):
            if cmd == 'lvl':
                selector = msUtil.lvl_functor
                if not value.isdigit():
                    value = 0
            elif cmd == 'atk':
                selector = msUtil.atk_functor
                if not value.isdigit():
                    value = 0
            elif cmd == 'def':
                selector = msUtil.def_functor
                value = (0, int(value))[value.isdigit()]
            elif cmd == 'usr':
                selector = msUtil.get_by_username_functor
            elif cmd == 'squad':
                selector = msUtil.is_in_squad_functor
            elif cmd == 'wosquad':
                selector = msUtil.not_in_squad_functor

            if selector is not None:
                matched_players += get_player_list_by_criteria(selector, value)
                for player in matched_players:
                    if len(answer) > 3000:
                        splitted_answer.append(answer)
                        answer = ""
                    answer += player.getAsLine()
        else:
            answer = 'Вы неверно сформировали запрос'
    else:
        answer = settings.accessDeniedMessage
    splitted_answer.append(answer)
    for text in splitted_answer:
        bot.send_message(message.chat.id, text)


@bot.message_handler(commands=['change'])
def handle_change_command(message):
    log(message)
    if restrictions.whiteList.get(str(message.from_user.id)):
        params = message.text.split(' ')
        del params[0]
        if len(params) is 0:
            dest = "oops"
        else:
            dest = (params.pop(0)).lower()

        if dest == "greeting":
            settings.greetingsMessage = ' '.join(params)
            answer = "Приветствие изменено"
        elif dest == "help":
            settings.helpMessage = ' '.join(params)
            answer = "Справка изменена"
        elif dest == "profileok":
            settings.profileAnswerOkMessage = ' '.join(params)
            answer = "Реакция на правильный профиль изменена"
        elif dest == "profileerr":
            settings.profileAnswerErrorMessage = ' '.join(params)
            answer = "Реакция на неправильный форвард профиля изменена"
        elif dest == "profilenotred":
            settings.profileAnswerWrongFortressMessage = ' '.join(params)
            answer = "Реакция на профиль из неправильного замка изменена"
        else:
            answer = "Неправильный параметр для изменения.\nДоступны следующие параметры:\n\t/change greeting - " \
                     "изменение фразы на команду /start\n\t/change help - изменение фразы на команду /help\n\t/change " \
                     "profileOK - изменение фразы на правильныый профиль\n\t/change profileErr - изменение фразы на " \
                     "неправильный профиль "
    else:
        answer = settings.accessDeniedMessage
    bot.send_message(message.chat.id, answer)


@bot.message_handler(content_types=['text'])
def handle_text(message):
    log(message)
    if message.forward_from is not None:
        if message.forward_from.id == constants.chatWarsID:
            player = playerClass.Player()
            if player.parseMessage(message):
                answer = settings.profileAnswerOkMessage
                is_spy = False
                if not player.fraction:
                    answer = settings.profileAnswerWrongFortressMessage
                else:
                    for i in spy_list:
                        if player.telegramID == i.telegramID:
                            is_spy = True
                    for i in playerList:
                        if player.telegramID == i.telegramID:
                            player.squad = i.squad
                            playerList.remove(i)
                    if is_spy:
                        print("Spy trying to send profile")
                    else:
                        playerList.append(player)
                        db.add_player(player)
                player.printPlayer()
            else:
                answer = settings.profileAnswerTimedOutProfile
        else:
            answer = settings.profileAnswerErrorMessage
        bot.send_message(message.chat.id, answer)
    else:
        if settings.playersCommand in message.text:
            handle_players_command(message)
        elif settings.statCommand in message.text:
            message.text = message.text.replace(settings.statCommand, "/getstat")
            message.text = message.text.replace("атаки", "attack")
            message.text = message.text.replace('защиты', 'def')
            message.text = message.text.replace("по золоту", "gold")
            message.text = message.text.replace("уровней", "level")
            handle_getstat_command(message)
        elif settings.retextCommand in message.text:
            message.text = message.text.replace(settings.retextCommand, '/change')
            handle_change_command(message)
        elif settings.actionsCommand in message.text:
            message.text = message.text.replace(settings.actionsCommand, '/attack')
            handle_attack_command(message)
        elif settings.actionAttackBlue in message.text:
            message.text = '/attack_blue'
            handle_defined_attacks_command(message)
        elif settings.actionAttackYellow in message.text:
            message.text = '/attack_yellow'
            handle_defined_attacks_command(message)
        elif settings.actionAttackWhite in message.text:
            message.text = '/attack_white'
            handle_defined_attacks_command(message)
        elif settings.actionAttackBlack in message.text:
            message.text = '/attack_black'
            handle_defined_attacks_command(message)
        elif settings.actionAttackMountainsFort in message.text:
            message.text = '/attack_mountain_fort'
            handle_defined_attacks_command(message)
        elif settings.actionAttackForestFort in message.text:
            message.text = '/attack_forest_fort'
            handle_defined_attacks_command(message)
        elif settings.actionDefense in message.text:
            message.text = '/def_castle'
            handle_defined_attacks_command(message)
        elif settings.actionDefenseFriend in message.text:
            message.text = '/def_friend_castle'
            handle_defined_attacks_command(message)
        elif settings.actionDefenseFort in message.text:
            message.text = '/def_fort'
            handle_defined_attacks_command(message)
        elif settings.backCommand in message.text:
            message.text = message.text.replace(settings.backCommand, "/back", 1)
            handle_back_command(message)
        elif settings.broadcastCommand in message.text:
            message.text = message.text.replace(settings.broadcastCommand, "/broadcast")
            handle_broadcast_command(message)
        elif settings.kickCommand in message.text:
            message.text = message.text.replace(settings.kickCommand, "/kick")
            handle_kick_command(message)
        elif settings.deleteAdminCommand in message.text:
            message.text = message.text.replace(settings.deleteAdminCommand, '/deleteadmin')
            handle_deleteadmin_command(message)
        elif settings.acceptActionCommand in message.text:
            message.text = message.text.replace(settings.acceptActionCommand, '/accept')
            handle_action_recievement_command(message)
        elif settings.declineActionCommand in message.text:
            message.text = message.text.replace(settings.declineActionCommand, '/decline')
            handle_action_recievement_command(message)
        elif settings.listAccepted in message.text:
            message.text = message.text.replace(settings.listAccepted, '/accepted')
            handle_players_decision_command(message)
        elif settings.listDeclined in message.text:
            message.text = message.text.replace(settings.listDeclined, '/declined')
            handle_players_decision_command(message)
        elif settings.setClass in message.text:
            answer = "Тут вы можете установить класс своего персонажа" \
                     "!!! Прошу обратить внимание, что это можно сделать ТОЛЬКО один раз"
            bot.send_message(message.chat.id, answer, reply_markup=setClassMarkup)
        elif settings.warriorClass in message.text:
            message.text = "/iamattacker"
            handle_iamattacker_command(message)
        elif settings.defenderClass in message.text:
            message.text = "/iamdefender"
            handle_iamdefender_command(message)
        elif settings.myProfile in message.text:
            message.text = "/me"
            handle_me_command(message)
        elif settings.allBroadcastsCommand in message.text:
            message.text = message.text.replace(settings.allBroadcastsCommand, '/listbroadcasts')
            handle_listbroadcasts_command(message)
        elif settings.spycastCommand in message.text:
            message.text = message.text.replace(settings.spycastCommand, '/spycast')
            handle_spycast_broadcast_command(message)
        elif settings.lowlevelcastCommand in message.text:
            message.text = message.text.replace(settings.lowlevelcastCommand, '/lowlevel')
            handle_lowlevel_broadcast_command(message)
        elif settings.midlevelcastCommand in message.text:
            message.text = message.text.replace(settings.midlevelcastCommand, '/midlevel')
            handle_lowlevel_broadcast_command(message)
        elif settings.highlevelcastCommand in message.text:
            message.text = message.text.replace(settings.highlevelcastCommand, '/highlevel')
            handle_lowlevel_broadcast_command(message)
        elif settings.insquadcastCommand in message.text:
            message.text = message.text.replace(settings.insquadcastCommand, '/squad')
            handle_squad_command(message)
        elif settings.allsquadscastCommand in message.text:
            message.text = message.text.replace(settings.allsquadscastCommand, '/allsquads')
            handle_allsquads_command(message)
        else:
            bot.send_message(message.chat.id, settings.unknownCommand)


bot.polling(none_stop=True, interval=False)
