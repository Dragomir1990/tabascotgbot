# README #

In this repo you may find source code for telergam bot, which help coordinate players of one fraction, and one attack-party.

### How do I get set up? ###

* To launch this bot you need to:
* Install python3
* Install pip3
* Install pyTelegramBotApi from pip
* In constants.py set your bot token
* Launch main.py

### Contribution guidelines ###

* List of developers, reviewers and QA will be here soon
### Who do I talk to? ###

You may contact @Drake1990 or @kotlet_tv in telegram messenger
If any issue observed - feel free to create issues with description(which type of defect found, log from console and/or log file, which commit was used as head, steps to reproduce)